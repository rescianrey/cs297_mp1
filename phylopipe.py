#   Authors: Madel Tutor (2011-11317), Xavier Asuncion (2010-24672), Rescian Rey (2008-14095), Pauline Dela Pena
#   Description: Command line interface for performing multiple alignment
#                for a sequence which will be specified by the user.
#   Language/Interpreter: Python 2.7

import os
import re
import shutil
import sys
import xml.etree.ElementTree as ET

from Bio import Entrez
from Bio.Blast import NCBIWWW
from Bio.Align.Applications import ClustalwCommandline


BASE_DIR = os.path.dirname(os.path.abspath(__file__))
EXE_DIR = os.path.join(BASE_DIR, 'exe')
VALID_ACCESSTION_NUMBER_FORMAT = {
    'nucleotide': r"^((\w{1}\d{5})|(\w{2}\d{6}))$",
    'protein': r"^\w{3}\d{5}$",
    'wgs': r"^\w{4}\d{8,10}$",
    'mga': r"^\w{5}\d{7}$"
}

OUTPUT_DIRECTORY = 'results'
SEQUENCE_FILE = 'sequence.fasta'
PHY_FILE = 'sequence.phy'
FASTA_SEQUENCE = os.path.join(BASE_DIR, OUTPUT_DIRECTORY, SEQUENCE_FILE)
E_VALUE = 0.001
PERCENT_IDENTITY = 98

PHY_TREE_CONSTRUCTION_OPTS = {
    '1': 'Distance-matrix methods',
    '2': 'Maximum likelihood'
}

# main
def main():
    # Get accession number/s
    query = []
    is_input_valid = True
    hit_count = None;

    welcome = '''
    *******************************************************************
    ****************** Welcome to PHYLO PIPE **************************
    *******************************************************************
    '''
    print welcome

    while not (query and is_input_valid):
        query = re.split(',\s*', raw_input("Enter accession number/s (comma-separated): "))
        query = set(query)

        if not query:
            print 'No accession number entered.'

        # Check if accession number/s entered is/are valid
        print ''
        print 'Checking if inputs are valid...'
        is_input_valid = True
        for number in query:
            is_input_valid = is_input_valid and is_valid(number)
        print 'Done'

    if len(query) == 1:
        # Asks for hits to return if given only one accession number
        while not hit_count:
            try:
                hit_count = int(raw_input('Enter the number of hits to return: '))
            except ValueError, e:
                print str(e)

    # Fetch Fasta
    print ''
    download_fasta(query, FASTA_SEQUENCE)

    if hit_count:
        query = run_blast(hit_count, FASTA_SEQUENCE)

        if len(query) <= 1:
            print 'Hits not enough for multiple alignment. Exiting.'
            sys.exit(1)
        download_fasta(query, FASTA_SEQUENCE)


    # Perform multiple alignment
    try:
        cline = ClustalwCommandline("clustalw", infile=FASTA_SEQUENCE, seqnos="ON", output='phylip', gapopen=2, gapext=0.5)
        cline()
    except Exception, err:
        print 'Error running clustal, make sure you have command-line clustalw installed. Exiting.'
        sys.exit(0)

    phy_path = os.path.join(OUTPUT_DIRECTORY, PHY_FILE)
    print ''
    # Perform phytree reconstruction
    phytree_reconstruction(phy_path, OUTPUT_DIRECTORY)
        

# Description:  Method to download fasta-formatted sequence.
# Input:        query - the list of accession numbers to fetch
#               sequence_path - the path where to store the fasta file
# Return:       None
def download_fasta(query, sequence_path):
    Entrez.email = "madeltutor@gmail.com"
    print 'Downloading FASTA file...'
    try:
        request = Entrez.efetch(db="nucleotide", id=" ".join(query), rettype = "fasta", retmode = "text")
        output_file = open(sequence_path, 'w+')
        output_file.write(request.read())
        output_file.close()
        request.close()
    except Exception, err:
        print 'Error on connection, please try again. Exiting.'
        sys.exit(0)
    print 'Done'


# Description:  Performs online Blast search
# Input:        hit_count - hits to fetch
#               sequence_path - path to where to get the sequence from which the search will be based. 
# Return:       Returns the accession numbers of the hits
def run_blast(hit_count, sequence_path):
    fileInput = open(sequence_path).read()
    result_root = None
    print 'Performing BLAST...'
    try:
        request = NCBIWWW.qblast("blastn", "nr", fileInput, hitlist_size=hit_count, expect=E_VALUE, perc_ident=PERCENT_IDENTITY, format_type="XML")
        result_root = ET.fromstring(request.read())
        request.close()
    except Exception, err:
        print 'Error on connection, please try again. Exiting.'
        sys.exit(0)

    query = []

    hits = result_root.find('BlastOutput_iterations').find('Iteration').find('Iteration_hits').findall('Hit')
    print 'Hits (%s):' % len(hits)
    for i, hit in enumerate(hits):
        print '[%d] %s' % (i+1, hit.find('Hit_id').text)
        query.append(hit.find('Hit_accession').text)

    print 'Done'
    return set(query)


# Description:      Performs phylogenetic tree reconstruction
# Input:            phy_path - path of the phy file generated by clustal
#                   output_directory - directory where to output results
# Return:           None
def phytree_reconstruction(phy_path, output_directory):
    options = '\n'.join(['[%s] %s' % (key, PHY_TREE_CONSTRUCTION_OPTS[key]) for key in PHY_TREE_CONSTRUCTION_OPTS])
    choice = ''

    # Ask how to perform phylogenetic tree reconstruction
    while choice not in PHY_TREE_CONSTRUCTION_OPTS:
        choice = raw_input('How would you like to perform phylogenetic tree reconstruction?\n%s\n\nEnter the number of your choice: ' % options)


    # Move phy file to exe folder
    shutil.copy(phy_path, os.path.join(BASE_DIR, 'infile'))
    output_files = []
    if PHY_TREE_CONSTRUCTION_OPTS[choice] == 'Distance-matrix methods':
        # Run dnadist
        distance_matrix_outfile = os.path.join(output_directory, 'distance_matrix_outfile')
        neighbor_outfile = os.path.join(output_directory, 'neighbor_outfile')
        neighbor_outtree = os.path.join(output_directory, 'neighbor_outtree')
        os.system(os.path.join(EXE_DIR,'dnadist'))
        os.rename("./outfile", "infile")

        # Run Neighbor
        os.system(os.path.join(EXE_DIR,'neighbor'))
        shutil.move("./infile", distance_matrix_outfile)
        shutil.move("./outfile", neighbor_outfile)
        shutil.move("./outtree", neighbor_outtree)

        output_files = [distance_matrix_outfile, neighbor_outfile, neighbor_outtree]
    else:
        # Run dnaml
        ml_outfile = os.path.join(output_directory, 'ml_outfile')
        ml_outtree = os.path.join(output_directory, 'ml_outtree')
        os.system(os.path.join(EXE_DIR,'dnaml'))
        shutil.move("./outfile", ml_outfile)
        shutil.move("./outtree", ml_outtree)

        output_files = [ml_outfile, ml_outtree]
        os.remove(os.path.join(BASE_DIR, 'infile'))
    print 'Output files:\n\t%s' % '\n\t'.join(output_files)


# Description: Determines if a given accession number is a valid one
#              Valid format is defined at http://www.ncbi.nlm.nih.gov/Sequin/acc.html
# Input:       accession_number - Accesstion number to verify
# Return:      Boolean value, true if valid, false otherwise.
def is_valid(accession_number):
    valid = False
    _type = ''

    for key in VALID_ACCESSTION_NUMBER_FORMAT:
        pattern = VALID_ACCESSTION_NUMBER_FORMAT[key];
        if re.match(pattern, accession_number):
            valid = True
            _type = key

    if valid:
        print '\t%s is a valid accession number.' % accession_number
    else:
        print '\t%s is an invalid accession number.' % accession_number
    return valid


if __name__ == '__main__':
    main()
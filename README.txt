PREREQUISITE:
- Install clustalw and add to your path if running in Windows. Install using 'sudo apt-get install clustalw' if in Linux
- pip install -r requirements.txt


USAGE:
- go to project directory
- to run, type: python phylopipe.py
- output files will be in results folder